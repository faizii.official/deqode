Hey there, i am Faizan Quazi. 

this is my project i've done in my training phase by Deqode. 

 
1)what this project does? 

Ans: -this project executes the commands in the containers terminal via Frontend and returns the output in Frontend UI page. 

 
2) how to try this project. 

Ans: -you can simply try this project with Cloud and Local environment 

------------------------------------------------------------------------------- 

1.deployment via Docker. 

step 1. install docker with command bellow. 

for Debian "sudo apt install docker.io" without “ 

for fedora "sudo yum install docker" 

step 2. start docker Deamon with command bellow. 

"sudo systemctl docker start" 

step 3. create a docker network 

command: - "sudo docker network create xyz" 

step 3. run the main app container and sidecar database (Redis container) 

command: -"docker run -d --name deqode --network xyz -p 80:8080 dexterquazi/deqode" 

command: -"docker run -d --name db --network xyz -p 6379:6379 redis" 

step 4: - visit the localhost port 80 in your browser or visit this URL http://127.0.0.1 

 

 

 

 

 

 

 

 

 

 

2.automated deployment in AWS EKS cluster. 

requirement, 

1.aws_access_key_id  

2.aws_secret_access_key  

3.region name example "us-east-2" 

 
Here is the steps to perform automated cluster formation and automated deployment in EKS cluster. 

 
 

step 1. 

open your terminal. 

step 2. 

command: - “git clone https://gitlab.com/faizii.official/deqode.git “ 

step 3. 

command: - “cd deqode” 

step 4. 

command: -"bash auto_create.sh" 

step 5. 

paste your aws_access_key_id and press enter 

paste your aws_secret_access_key and press enter 

enter the region in which you want to create EKS cluster. example: - us-east-2 

step 6. sit back and relax everything will be done automatically. 

 

after completion of task 

you will see AWS load balancer address in your terminal simply hit that  URL to visit the app. 

 

Thank you. 

 
 
 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
