import subprocess

from flask import Flask, request, render_template
import os


app = Flask(__name__)

@app.route('/')
def my_form():
    val1 = subprocess.Popen("redis-cli get key", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    arg = val1.decode('utf-8')
    return render_template('index.html',data="hey",db=arg)



@app.route('/', methods=['POST'])

def my_form_post():
    
    text = request.form['text']
    l = subprocess.Popen(text, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    p = l.decode('utf-8')
    data = '<p>'+  p + '</p>'

    val = subprocess.Popen("redis-cli set key " + text, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    val1 = subprocess.Popen("redis-cli get key " , shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    arg = val1.decode('utf-8')
    hey = '<p>'+  arg + '</p>'
    return render_template('index.html', data=data ,db=hey)

                                                                                                                                                                                                                                       
app.run(debug=True , host="0.0.0.0" , port=5000)                                                                                                                                                                                                                                                                                                                          
