resource "aws_instance" "web" {
  ami           = "ami-0fa49cc9dc8d62c84"
  instance_type = "t2.micro"
  subnet_id = "subnet-08dd4599db9349f66"
  security_groups = ["sg-0732789b019ddc473"]
  user_data = <<-EOL
  #!/bin/bash -xe
sudo yum install docker -y
sudo systemctl start docker 
sudo docker run -it --name deploy dexterquazi/deploy
  EOL

  tags = {
    Name = "HelloWorld"
  }
}
