rm -rf credentials  config create_cluster_with_terraform
git clone https://github.com/hashicorp/learn-terraform-provision-eks-cluster  create_cluster_with_terraform
echo "enter the aws account accesskey id"
read i
echo "enter the aws account secret key"
read k
cat <<EOF >> credentials
[default]
aws_access_key_id = $i
aws_secret_access_key = $k
EOF
echo "enter the region in which you want to create cluster"
read arg
cat <<EOF >> config
[default]
output = json
region = $arg
EOF
sudo apt-get install docker.io -y>/dev/null
sudo yum install docker -y> /dev/null 
clear
echo "installing terraform"
sudo cp terraform /bin/terraform
sudo chmod 777 /bin/terraform
echo installing kubectl 
sudo cp kubectl /bin/kubectl
chmod 777 /bin/kubectl
echo installing eksctl 
sudo cp eksctl /bin/eksctl
sudo chmod 777 /bin/eksctl
clear
echo init terraform
git clone https://github.com/hashicorp/learn-terraform-provision-eks-cluster  create_cluster_with_terraform
cd create_cluster_with_terraform
terraform init
clear
echo "performing terraform apply"
sleep 4
terraform apply 

sleep 5
echo "done createing cluster now building supporting image to perform deployment of app make sure you have docker installed"
cd ..
sudo docker build . -t deployment
sudo docker run --rm -it deployment
clear
echo "done deployment in cluster getting public address of service attched to deployment"
sleep 4
sudo docker run --rm -it deployment kubectl get svc
